from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import CreateProjectForm


# Create your views here.


@login_required
def Project_ListView(request):
    project_list = Project.objects.filter(owner=request.user)
    context = {
        "project_list": project_list,
    }
    return render(request, "projects/list.html", context)


@login_required
def Project_DetailView(request, id):
    project_detail = get_object_or_404(Project, id=id)
    context = {
        "project_detail": project_detail,
    }
    return render(request, "projects/detail.html", context)


@login_required
def Project_CreateView(request):
    if request.method == "GET":
        form = CreateProjectForm()
    else:
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
