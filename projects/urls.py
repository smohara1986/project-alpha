from django.urls import path
from projects.views import (
    Project_ListView,
    Project_DetailView,
    Project_CreateView,
)


urlpatterns = [
    path("", Project_ListView, name="list_projects"),
    path("<int:id>/", Project_DetailView, name="show_project"),
    path("create/", Project_CreateView, name="create_project"),
]
