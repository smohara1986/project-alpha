from django.urls import path
from tasks.views import Task_CreateView, Task_ListView


urlpatterns = [
    path("create/", Task_CreateView, name="create_task"),
    path("mine/", Task_ListView, name="show_my_tasks"),
]
