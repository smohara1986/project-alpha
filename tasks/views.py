from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTaskForm
from tasks.models import Task


# Create your views here.


@login_required
def Task_CreateView(request):
    if request.method == "GET":
        form = CreateTaskForm()
    else:
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def Task_ListView(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {
        "task_list": task_list,
    }
    return render(request, "tasks/list.html", context)
